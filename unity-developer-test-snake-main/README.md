# unity-developer-test-snake

Existing code improvement test.

The goal of this test is to improve and modernise the existing code base to make it suitable for a live game.

You are asked to focus on those areas (prioritized):
1. Memory Management. //
2. Making code easier to test, and adding tests. //
3. In the future, we plan to start adding LiveOps to the game, so we will need to be able to update the game assets and config without requiring a new release.
4. Improve the maintainability and extendability of the code. //
5. Reduce compile times. //
6. In the future, we are considering releasing it on different platforms that might have different controls schemes. //

As there might be too much to be improved in this test, it should be taken as a time box. So:

- List the improvements that you can see as appliable to the test.
- Pick and implement the most important ones that you think you can do in the given time.
- Explain your choices on which to pick.


Please send back a zip file containing the whole project and the git history. We are not that much interested on the actual commits, but it makes it easier for us to review the test 🙂

This project is originally from Unity Code Monkey and has plenty of mentions of that all over. The original video can be seen here:
https://unitycodemonkey.com/video.php?v=ctCa1rH3CIc



Look athe code baes //

Change GO.Find  //

Look at the Update function //

Add pool for the apple //

Make the apple and the snake prefabs // 

Load them at the start of the game //

Add scriptanle Obj for the config//

Change logic to suport that (SO)//

Different platforms//

Add tests //

Change the sound system //

Bug (food is invisible) //

Change audio logic //