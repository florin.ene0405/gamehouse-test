﻿/* 
    ------------------- Code Monkey -------------------

    Thank you for downloading this package
    I hope you find it useful in your projects
    If you have any questions let me know
    Cheers!

               unitycodemonkey.com
    --------------------------------------------------
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CodeMonkey;
using CodeMonkey.Utils;

public class GameHandler : MonoBehaviour
{
	public static GameHandler Instance;

	[HideInInspector]public Snake Snake;

	public LevelGrid LevelGrid;

	public GameDataScriptableObject GameData;

	private void Awake()
	{
		if (Instance == null)
		{
			Instance = this;
		}
		else
		{
			Destroy(this);
		}

	

		Snake = Instantiate<Snake>(GameAssets.Instance.SnakePrefab);

		Snake.ResumeGame();
	}

	private void Start()
	{
		Debug.Log("GameHandler.Start");

		if (GameData == null)
		{
			Debug.LogError("GameData is null in - " + this);
			return;
		}
		Score.InitializeStatic(GameData.ScoreOnEat);

		LevelGrid = new LevelGrid(GameData.GridSize.x, GameData.GridSize.y);

		Snake.Setup(LevelGrid, GameData.SnakeSpeed);
		LevelGrid.Setup(Snake);
	}

	private void Update()
	{
		if (Input.GetKeyDown(KeyCode.Escape))
		{
			if (IsGamePaused())
			{
				GameHandler.Instance.ResumeGame();
			}
			else
			{
				GameHandler.Instance.PauseGame();
			}
		}
	}

	public static void SnakeDied()
	{
		bool isNewHighscore = Score.TrySetNewHighscore();
		GameOverWindow.ShowStatic(isNewHighscore);
		ScoreWindow.HideStatic();
	}

	public void ResumeGame()
	{
		PauseWindow.Instance.Hide();
		Snake.ResumeGame();
	}

	public void PauseGame()
	{
		PauseWindow.Instance.Show();
		Snake.PauseTheGame();
	}

	public  bool IsGamePaused()
	{
		return Snake.IsPaused;
	}
}
