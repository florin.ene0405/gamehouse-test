﻿/* 
    ------------------- Code Monkey -------------------

    Thank you for downloading this package
    I hope you find it useful in your projects
    If you have any questions let me know
    Cheers!

               unitycodemonkey.com
    --------------------------------------------------
 */

using UnityEngine;

public class LevelGrid
{
	private Vector2Int foodGridPosition;
	private SpriteRenderer foodGameObject;
	private int width;
	private int height;
	private Snake snake;

	public LevelGrid(int width, int height)
	{
		this.width = width;
		this.height = height;
	}

	public void Setup(Snake snake)
	{
		this.snake = snake;

		SpawnFood();
	}

	public void SpawnFood()
	{
		if (foodGameObject == null)
		{
			foodGameObject = Object.Instantiate<SpriteRenderer>(GameAssets.Instance.FoodPrefab);
		}

		do
		{
			foodGridPosition = new Vector2Int(Random.Range(0, width), Random.Range(0, height));
		}

		while (snake.GetFullSnakeGridPositionList().IndexOf(foodGridPosition) != -1);

		foodGameObject.transform.position = new Vector3(foodGridPosition.x, foodGridPosition.y, -2);
	}

	public bool TrySnakeEatFood(Vector2Int snakeGridPosition)
	{
		if (snakeGridPosition == foodGridPosition)
		{
			SpawnFood();
			Score.AddScore();
			return true;
		}
		else
		{
			return false;
		}
	}

	public void EatFakeFood()
	{
		SpawnFood();
		Score.AddScore();
	}

	public Vector2Int ValidateGridPosition(Vector2Int gridPosition)
	{
		if (gridPosition.x < 0)
		{
			gridPosition.x = width - 1;
		}
		if (gridPosition.x > width - 1)
		{
			gridPosition.x = 0;
		}
		if (gridPosition.y < 0)
		{
			gridPosition.y = height - 1;
		}
		if (gridPosition.y > height - 1)
		{
			gridPosition.y = 0;
		}
		return gridPosition;
	}

	public Vector2Int GetWidthAndHeight()
	{
		return new Vector2Int(width, height);
	}
}
