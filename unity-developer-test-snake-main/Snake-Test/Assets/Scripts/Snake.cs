﻿/* 
    ------------------- Code Monkey -------------------

    Thank you for downloading this package
    I hope you find it useful in your projects
    If you have any questions let me know
    Cheers!

               unitycodemonkey.com
    --------------------------------------------------
 */

using System.Collections.Generic;
using UnityEngine;

public class Snake : MonoBehaviour
{
	private bool _isPaused = false;
	public bool IsPaused { get { return _isPaused; } }

	private enum Direction
	{
		Left,
		Right,
		Up,
		Down
	}

	private enum State
	{
		Alive,
		Dead
	}

	private State state;
	private Direction gridMoveDirection;
	private Vector2Int gridPosition;
	private float gridMoveTimer;
	private float gridMoveTimerMax;
	private LevelGrid levelGrid;
	private int snakeBodySize;
	private List<SnakeMovePosition> snakeMovePositionList;
	private List<SnakeBodyPart> snakeBodyPartList;

	private Vector3 fp;
	private Vector3 lp;
	private float dragDistance;

	public void Setup(LevelGrid levelGrid , float speed = .2f)
	{
		this.levelGrid = levelGrid;
		gridMoveTimerMax = speed;
		gridMoveTimer = gridMoveTimerMax;
		gridPosition = this.levelGrid.GetWidthAndHeight();
	}

	private void Awake()
	{
		gridPosition = new Vector2Int(10, 10);
		gridMoveDirection = Direction.Right;

		snakeMovePositionList = new List<SnakeMovePosition>();
		snakeBodySize = 0;

		snakeBodyPartList = new List<SnakeBodyPart>();

		state = State.Alive;
	}
	private void Start()
	{
		dragDistance = Screen.height * 15 / 100;
	}

	public void PauseTheGame()
	{
		_isPaused = true;
	}

	public void ResumeGame()
	{
		_isPaused = false;
	}

	private void Update()
	{
		if (!_isPaused && state == State.Alive)
		{
			HandleInput();
			HandleGridMovement();
		}
	}

	private void HandleInput()
	{
		if (Application.platform == RuntimePlatform.WindowsPlayer || Application.platform == RuntimePlatform.WindowsEditor || Application.platform == RuntimePlatform.WebGLPlayer)
		{
			StandaloneInput();
		}
		else if (Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.IPhonePlayer)
		{
			PhoneInput();
		}
	}

	private void PhoneInput()
	{
		if (Input.touchCount == 1)
		{
			Touch touch = Input.GetTouch(0);
			if (touch.phase == TouchPhase.Began)
			{
				fp = touch.position;
				lp = touch.position;
			}
			else if (touch.phase == TouchPhase.Moved)
			{
				lp = touch.position;
			}
			else if (touch.phase == TouchPhase.Ended)
			{
				lp = touch.position;


				if (Mathf.Abs(lp.x - fp.x) > dragDistance || Mathf.Abs(lp.y - fp.y) > dragDistance)
				{

					if (Mathf.Abs(lp.x - fp.x) > Mathf.Abs(lp.y - fp.y))
					{
						if ((lp.x > fp.x))
						{
							MoveRight();
							Debug.Log("Right Swipe");
						}
						else
						{
							MoveLeft();
							Debug.Log("Left Swipe");
						}
					}
					else
					{
						if (lp.y > fp.y)
						{
							MoveUp();
							Debug.Log("Up Swipe");
						}
						else
						{
							MoveDown();
							Debug.Log("Down Swipe");
						}
					}
				}
			}
		}
	}

	private void StandaloneInput()
	{
		if (Input.GetKeyDown(KeyCode.UpArrow) || Input.GetKeyDown(KeyCode.W))
		{
			MoveUp();
		}
		if (Input.GetKeyDown(KeyCode.DownArrow) || Input.GetKeyDown(KeyCode.S))
		{
			MoveDown();
		}
		if (Input.GetKeyDown(KeyCode.LeftArrow) || Input.GetKeyDown(KeyCode.A))
		{
			MoveLeft();
		}
		if (Input.GetKeyDown(KeyCode.RightArrow) || Input.GetKeyDown(KeyCode.D))
		{
			MoveRight();
		}
	}

	private void MoveUp()
	{
		if (gridMoveDirection != Direction.Down)
		{
			gridMoveDirection = Direction.Up;
		}
	}

	private void MoveDown()
	{
		if (gridMoveDirection != Direction.Up)
		{
			gridMoveDirection = Direction.Down;
		}
	}

	private void MoveRight()
	{
		if (gridMoveDirection != Direction.Left)
		{
			gridMoveDirection = Direction.Right;
		}
	}

	private void MoveLeft()
	{
		if (gridMoveDirection != Direction.Right)
		{
			gridMoveDirection = Direction.Left;
		}
	}

	private void HandleGridMovement()
	{
		gridMoveTimer += Time.deltaTime;
		if (gridMoveTimer >= gridMoveTimerMax)
		{
			gridMoveTimer -= gridMoveTimerMax;

			//SoundManager.PlaySound(SoundManager.Sound.SnakeMove);

			SnakeMovePosition previousSnakeMovePosition = null;
			if (snakeMovePositionList.Count > 0)
			{
				previousSnakeMovePosition = snakeMovePositionList[0];
			}

			SnakeMovePosition snakeMovePosition = new SnakeMovePosition(previousSnakeMovePosition, gridPosition, gridMoveDirection);
			snakeMovePositionList.Insert(0, snakeMovePosition);

			Vector2Int gridMoveDirectionVector;
			switch (gridMoveDirection)
			{
				default:
				case Direction.Right: gridMoveDirectionVector = new Vector2Int(+1, 0); break;
				case Direction.Left: gridMoveDirectionVector = new Vector2Int(-1, 0); break;
				case Direction.Up: gridMoveDirectionVector = new Vector2Int(0, +1); break;
				case Direction.Down: gridMoveDirectionVector = new Vector2Int(0, -1); break;
			}

			gridPosition += gridMoveDirectionVector;

			gridPosition = levelGrid.ValidateGridPosition(gridPosition);

			if (levelGrid.TrySnakeEatFood(gridPosition))
			{
				AteFoot();
			}

			if (snakeMovePositionList.Count >= snakeBodySize + 1)
			{
				snakeMovePositionList.RemoveAt(snakeMovePositionList.Count - 1);
			}

			UpdateSnakeBodyParts();

			foreach (SnakeBodyPart snakeBodyPart in snakeBodyPartList)
			{
				Vector2Int snakeBodyPartGridPosition = snakeBodyPart.GetGridPosition();
				if (gridPosition == snakeBodyPartGridPosition)
				{
					SnakeDied();
				}
			}

			transform.position = new Vector3(gridPosition.x, gridPosition.y);
			transform.eulerAngles = new Vector3(0, 0, GetAngleFromVector(gridMoveDirectionVector) - 90);
		}
	}

	public void SnakeDied()
	{
		state = State.Dead;
		GameHandler.SnakeDied();
		SoundManager.PlaySound(SoundManager.Sound.SnakeDie);
	}

	public void AteFoot()
	{
		snakeBodySize++;
		CreateSnakeBodyPart();
		SoundManager.PlaySound(SoundManager.Sound.SnakeEat);
	}

	private void CreateSnakeBodyPart()
	{
		snakeBodyPartList.Add(new SnakeBodyPart());
	}

	private void UpdateSnakeBodyParts()
	{
		for (int i = 0; i < snakeBodyPartList.Count; i++)
		{
			snakeBodyPartList[i].SetSnakeMovePosition(snakeMovePositionList[i]);
		}
	}


	private float GetAngleFromVector(Vector2Int dir)
	{
		float n = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
		if (n < 0) n += 360;
		return n;
	}

	public Vector2Int GetGridPosition()
	{
		return gridPosition;
	}

	// Return the full list of positions occupied by the snake: Head + Body
	public List<Vector2Int> GetFullSnakeGridPositionList()
	{
		List<Vector2Int> gridPositionList = new List<Vector2Int>() { gridPosition };
		foreach (SnakeMovePosition snakeMovePosition in snakeMovePositionList)
		{
			gridPositionList.Add(snakeMovePosition.GetGridPosition());
		}
		return gridPositionList;
	}




	/*
     * Handles a Single Snake Body Part
     * */
	private class SnakeBodyPart
	{

		private SnakeMovePosition snakeMovePosition;
		private Transform transform;

		public SnakeBodyPart()
		{
			SpriteRenderer snakeBodyGameObject = Object.Instantiate<SpriteRenderer>(GameAssets.Instance.SnakeBodyPrefab);
			snakeBodyGameObject.sortingOrder = 1;
			transform = snakeBodyGameObject.transform;
		}

		public void SetSnakeMovePosition(SnakeMovePosition snakeMovePosition)
		{
			this.snakeMovePosition = snakeMovePosition;

			transform.position = new Vector3(snakeMovePosition.GetGridPosition().x, snakeMovePosition.GetGridPosition().y);

			float angle;
			switch (snakeMovePosition.GetDirection())
			{
				default:
				case Direction.Up: // Currently going Up
					switch (snakeMovePosition.GetPreviousDirection())
					{
						default:
							angle = 0;
							break;
						case Direction.Left: // Previously was going Left
							angle = 0 + 45;
							transform.position += new Vector3(.2f, .2f);
							break;
						case Direction.Right: // Previously was going Right
							angle = 0 - 45;
							transform.position += new Vector3(-.2f, .2f);
							break;
					}
					break;
				case Direction.Down: // Currently going Down
					switch (snakeMovePosition.GetPreviousDirection())
					{
						default:
							angle = 180;
							break;
						case Direction.Left: // Previously was going Left
							angle = 180 - 45;
							transform.position += new Vector3(.2f, -.2f);
							break;
						case Direction.Right: // Previously was going Right
							angle = 180 + 45;
							transform.position += new Vector3(-.2f, -.2f);
							break;
					}
					break;
				case Direction.Left: // Currently going to the Left
					switch (snakeMovePosition.GetPreviousDirection())
					{
						default:
							angle = +90;
							break;
						case Direction.Down: // Previously was going Down
							angle = 180 - 45;
							transform.position += new Vector3(-.2f, .2f);
							break;
						case Direction.Up: // Previously was going Up
							angle = 45;
							transform.position += new Vector3(-.2f, -.2f);
							break;
					}
					break;
				case Direction.Right: // Currently going to the Right
					switch (snakeMovePosition.GetPreviousDirection())
					{
						default:
							angle = -90;
							break;
						case Direction.Down: // Previously was going Down
							angle = 180 + 45;
							transform.position += new Vector3(.2f, .2f);
							break;
						case Direction.Up: // Previously was going Up
							angle = -45;
							transform.position += new Vector3(.2f, -.2f);
							break;
					}
					break;
			}

			transform.eulerAngles = new Vector3(0, 0, angle);
		}

		public Vector2Int GetGridPosition()
		{
			return snakeMovePosition.GetGridPosition();
		}
	}



	/*
     * Handles one Move Position from the Snake
     * */
	private class SnakeMovePosition
	{

		private SnakeMovePosition previousSnakeMovePosition;
		private Vector2Int gridPosition;
		private Direction direction;

		public SnakeMovePosition(SnakeMovePosition previousSnakeMovePosition, Vector2Int gridPosition, Direction direction)
		{
			this.previousSnakeMovePosition = previousSnakeMovePosition;
			this.gridPosition = gridPosition;
			this.direction = direction;
		}

		public Vector2Int GetGridPosition()
		{
			return gridPosition;
		}

		public Direction GetDirection()
		{
			return direction;
		}

		public Direction GetPreviousDirection()
		{
			if (previousSnakeMovePosition == null)
			{
				return Direction.Right;
			}
			else
			{
				return previousSnakeMovePosition.direction;
			}
		}

	}

}
