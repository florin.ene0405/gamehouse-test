﻿/* 
    ------------------- Code Monkey -------------------

    Thank you for downloading this package
    I hope you find it useful in your projects
    If you have any questions let me know
    Cheers!

               unitycodemonkey.com
    --------------------------------------------------
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CodeMonkey.Utils;

public class MainMenuWindow : MonoBehaviour
{
	[SerializeField]private RectTransform _howToPlaySub;
	[SerializeField]private RectTransform _mainSub;
	[SerializeField]private Button_UI _playBtn;
	[SerializeField]private Button_UI _howToPlayBtn;
	[SerializeField]private Button_UI _backBtn;
	[SerializeField] private Button_UI _quitBtn;

	private enum Sub
	{
		Main,
		HowToPlay,
	}

	private void Awake()
	{
		_howToPlaySub.anchoredPosition = Vector2.zero;
		_mainSub.anchoredPosition = Vector2.zero;

		_playBtn.ClickFunc = () => Loader.Load(Loader.Scene.GameScene);
		_playBtn.AddButtonSounds();

		_quitBtn.ClickFunc = () => Application.Quit();
		_quitBtn.AddButtonSounds();

		_howToPlayBtn.ClickFunc = () => ShowSub(Sub.HowToPlay);
		_howToPlayBtn.AddButtonSounds();

		_backBtn.ClickFunc = () => ShowSub(Sub.Main);
		_backBtn.AddButtonSounds();

		ShowSub(Sub.Main);
	}

	private void ShowSub(Sub sub)
	{
		_mainSub.gameObject.SetActive(sub == Sub.Main);
		_howToPlaySub.gameObject.SetActive(sub == Sub.HowToPlay);
	}
}
