using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tests : MonoBehaviour
{
	[SerializeField] private GameHandler _gameHandler;

	[ContextMenu("DeleteAllPlayerPrefs")]
	public void DeleteAllPlayerPrefs()
	{
		PlayerPrefs.DeleteAll();
	}

   [ContextMenu("SnakeLevelUp")]
	public void SnakeLevelUp()
	{
		_gameHandler.Snake.AteFoot();
	}

	[ContextMenu("AddScore")]
	public void AddScore()
	{
		Score.AddScore();
	}

	[ContextMenu("EatFakeFood")]
	public void EatFakeFood()
	{
		_gameHandler.LevelGrid.EatFakeFood();
		SnakeLevelUp();
	}


	[ContextMenu("MoveFood")]
	public void MoveFood()
	{
		_gameHandler.LevelGrid.SpawnFood();
	}

   [ContextMenu("KillSnake")]
	public void KillSnake()
	{
		_gameHandler.Snake.SnakeDied();
	}
}
