﻿/* 
    ------------------- Code Monkey -------------------

    Thank you for downloading this package
    I hope you find it useful in your projects
    If you have any questions let me know
    Cheers!

               unitycodemonkey.com
    --------------------------------------------------
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using CodeMonkey.Utils;

public class GameOverWindow : MonoBehaviour {

    private static GameOverWindow instance;

    [SerializeField] private Button_UI _retryBtn;
    [SerializeField] private GameObject _newHighscoreText;
    [SerializeField] private Text _scoreText;
    [SerializeField] private Text _highscoreText;

    private void Awake() {
        instance = this;

        _retryBtn.ClickFunc = () => { 
            Loader.Load(Loader.Scene.GameScene);
        };

        Hide();
    }

    private void Show(bool isNewHighscore) {
        gameObject.SetActive(true);

        _newHighscoreText.SetActive(isNewHighscore);

        _scoreText.text = Score.GetScore().ToString();
        _highscoreText.text = "HIGHSCORE " + Score.GetHighscore();
    }

    private void Hide() {
        gameObject.SetActive(false);
    }

    public static void ShowStatic(bool isNewHighscore) {
        instance.Show(isNewHighscore);
    }
}
