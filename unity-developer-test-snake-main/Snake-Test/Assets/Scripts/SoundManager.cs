﻿/* 
    ------------------- Code Monkey -------------------

    Thank you for downloading this package
    I hope you find it useful in your projects
    If you have any questions let me know
    Cheers!

               unitycodemonkey.com
    --------------------------------------------------
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CodeMonkey.Utils;

public static class SoundManager
{

	private static Dictionary<Sound, AudioClip> _audioClips = new Dictionary<Sound, AudioClip>();
	private static bool _isDictionaryGenerated = false;

	public enum Sound
	{
		SnakeMove,
		SnakeDie,
		SnakeEat,
		ButtonClick,
		ButtonOver,
	}

	private static AudioSource _soundObject;

	public static void PlaySound(Sound sound)
	{
		if (_soundObject == null)
		{
			GameObject newObject = new GameObject("Sound");
			_soundObject = newObject.AddComponent<AudioSource>();
		}

		_soundObject.PlayOneShot(GetAudioClip(sound));
	}

	private static AudioClip GetAudioClip(Sound sound)
	{
		if (!_isDictionaryGenerated)
		{
			GenerateTheDictionary();
		}

		_audioClips.TryGetValue(sound, out var soundToPlay);

		if (soundToPlay != null)
		{
			return soundToPlay;
		}
		else
		{
			Debug.LogError("Sound " + sound + " not found!");
			return null;
		}
	}

	public static void AddButtonSounds(this Button_UI buttonUI)
	{
		buttonUI.MouseOverOnceFunc += () => SoundManager.PlaySound(Sound.ButtonOver);
		buttonUI.ClickFunc += () => SoundManager.PlaySound(Sound.ButtonClick);
	}

	private static void GenerateTheDictionary()
	{
		var soundsArray = GameAssets.Instance.soundAudioClipArray;

		for (int i = 0; i < soundsArray.Length; i++)
		{
			_audioClips.TryGetValue(soundsArray[i].sound, out var sound);
			if (sound == null)
			{
				_audioClips.Add(soundsArray[i].sound, soundsArray[i].audioClip);
			}
		}
		_isDictionaryGenerated = true;
	}
}
