﻿/* 
    ------------------- Code Monkey -------------------

    Thank you for downloading this package
    I hope you find it useful in your projects
    If you have any questions let me know
    Cheers!

               unitycodemonkey.com
    --------------------------------------------------
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CodeMonkey.Utils;

public class PauseWindow : MonoBehaviour
{

	public static PauseWindow Instance;

	[SerializeField] private Button_UI _resumeBtn;
	[SerializeField] private Button_UI _mainMenuBtn;

	private void Awake()
	{
		if (Instance == null)
		{
			Instance = this;
		}
		else
		{
			Destroy(this);
		}

		var rect = transform.GetComponent<RectTransform>();

		rect.anchoredPosition = Vector2.zero;
		rect.sizeDelta = Vector2.zero;

		_resumeBtn.ClickFunc = () => GameHandler.Instance.ResumeGame();
		_resumeBtn.AddButtonSounds();

		_mainMenuBtn.ClickFunc = () => Loader.Load(Loader.Scene.MainMenu);
		_mainMenuBtn.AddButtonSounds();

		Hide();
	}

	public void Show()
	{
		gameObject.SetActive(true);
	}

	public void Hide()
	{
		gameObject.SetActive(false);
	}
}
