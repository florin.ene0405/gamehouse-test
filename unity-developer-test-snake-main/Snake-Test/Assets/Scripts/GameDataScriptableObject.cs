using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "GameData", menuName = "ScriptableObjects/GameDataScriptableObject", order = 1)]
public class GameDataScriptableObject : ScriptableObject
{
	[Header("The size of the Grid")]
	public Vector2Int GridSize;
	[Header("Points that you will get after eating an apple")]
	public int ScoreOnEat;
	[Header("Ths speed of the Snake (Lower is faster)")]
	public float SnakeSpeed;
}
